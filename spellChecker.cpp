/* * * * * * * * * ** * * * * * * * * * * */
/*  Author: Venetsia Krasteva 40313507    */
/*                                        */
/* Date of last modification : 16.04.2018 */
/*                                        */
/* Purpose of the program:                */
/*   Program to check spelling            */
/*                                        */
/* * * * * * * * * * * * * * * * * * * *  */

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>
#include <algorithm>
#include "bst.h" // including binary search tree header file
#include <vector>
#include "spellChecker.h"

using namespace std;



/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* Method for getting rid of punctuation and converting to lower case   *
*  							                                           *
*	Variables:                                                         *
*    - string str - passed in the function                             *
*	 - string noPunct - string that stores changed string              *
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * **/
string removePunc(string str)
{
	string noPunc = "";
	
	//loops through the string
	for(int i = 0; i < str.length(); i++)
	{
		//if the char isn't punctuation
		if(!ispunct(str[i]))
		{
			//char is converted to lower case
			noPunc += tolower(str[i]);
		}
	}
	return noPunc;
}


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* The main method that is taking the arguments and deciding what to do *
*  							                                           *
*	Variables:                                                         *
*    - argc - argument count                                           *
*	 - argv - argument vector                                          *
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * **/

int main(int argc, char** argv)
{
	//iFlag - indicated input file
	//dFlag - indicated dictionary file
	int   iFlag = 0, dFlag = 0;
	
	char dictionaryFile[50]; //input dictionary file
	char file[50]; // input file
	
	vector<string> wordsInput; // vector to store words from input file
	
	string wordsDictionary;
	string words;
	
	ifstream inputDictionaryFile; //Dictionary file input
	ifstream inputFile; // File input
	
	//if argc is 1
	if(argc == 1)
	{
		printf("Default settings\n");
	}// end if
	else
	{
		//go through argc
		for(int i=1; i<argc;i++)
		{
			//if -d is found
			if(strcmp(argv[i], "-d")==0)
			{
				//cout << "dictionary" << endl;;
				dFlag = 1;
				inputDictionaryFile.open(argv[++i]);
			}
			//if -i is found
			if(strcmp(argv[i], "-i")==0)
			{
				//cout << "input" << endl;;
				iFlag = 1;
				inputFile.open(argv[++i]);
			}
		}//end of for
	}// end of else
	
    //create a Binary Search Tree
	BinarySearchTree *tree = new BinarySearchTree();
	
	/* GETTING DICTIONARY*/
	
	//if no dictionary file from console line --> default 
	if(dFlag != 1)
	{
		//Promp user for dictionary file
		cout << "Enter dictionary file name: ";
		
		cin.getline(dictionaryFile,1000); // getting lines 
		
		inputDictionaryFile.open(dictionaryFile); //open
	}
	
	//if it fails to open - Error
	if(!inputDictionaryFile.is_open())
	{
		cout << "Fail to open file" << endl;
		exit(EXIT_FAILURE);
	}
	
	//Read the words in Dictionary
	while(inputDictionaryFile >> wordsDictionary)
	{
		//insert into tree without punctuation and in lower case 
		tree->insert(removePunc(wordsDictionary));
	}
	
	//check if tree is empty
	if(tree == nullptr)
	{
		cout << "Empty tree" << endl;
	}
	
	/* GETTING FILE*/
	
	//if no input file from console line
	if(iFlag != 1)
	{
		//Promp user for input file 
		cout << "Enter file name: ";
		
		cin.getline(file,1000); // getting lines 
		
		inputFile.open(file); //open
	}
	
	//if it fails to open - Error
	if(!inputFile.is_open())
	{
		cout << "Fail to open file" << endl;
		exit(EXIT_FAILURE);
	}
	
	//Read words in input file
	while(inputFile >> words)
	{	
		//insert into vector without punctuation and in lower case 
		wordsInput.push_back(removePunc(words));
	}
	
	//Go through vector 
	for(int i = 0; i < wordsInput.size(); i++)
	{
		//If a word from vector isn't in Binary Search Tree
		if(!tree->exists(wordsInput[i]))
		{
			cout <<"Misspelled: " << wordsInput[i] << endl;
		}
	}
	
	inputDictionaryFile.close();
	inputFile.close();
	delete tree;
	wordsInput.clear(); 	// calling clear will do the following:
						// 1) invoke the deconstrutor for every myclass
						// 2) size == 0 (the vector contained the actual objects).
}