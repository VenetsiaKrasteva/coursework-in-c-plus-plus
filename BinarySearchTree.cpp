#include "bst.h"
#include <cstdlib>

// Creates an empty binary tree
BinarySearchTree::BinarySearchTree()
{
	Node *root = new Node;
}

// Creates a binary tree with an initial word to store
BinarySearchTree::BinarySearchTree(std::string word)
{
	Node* node = new Node; //n for new
	node->data = word; 
	node->left = nullptr;
	node->right = nullptr;
	root = node;
}

Node* copyTree(const Node *tree1)
{
	if(tree1 == nullptr)
	{
		return nullptr;
	}
	else 
	{
		Node *tree2 = new Node;
		tree2->data = tree1->data;
		tree2->left = copyTree(tree1->left);
		tree2->right = copyTree(tree1->right);
		
		return tree2;
	}
} 

// Creates a binary tree by copying an existing tree
BinarySearchTree::BinarySearchTree(const BinarySearchTree &rhs)
{
	root = copyTree(rhs.root); 
}

//Deletes Tree
BinarySearchTree::~BinarySearchTree()
{
	//delete(root);
	if(root!=NULL)
    {
        delete(root->left);
        delete(root->right);
        delete(root);
        if(root->left!=NULL)
            root->left=NULL;
        if(root->right!=NULL)
            root->right=NULL;
        root=NULL;
    }
	
}

//Helper function to insert a word into the tree
void insertHelper(Node **node, std::string word)
{
	//Check if nullptr. If so set new node
	if(*node == nullptr)
	{
		//Create new node
		*node = new Node;
		//Set new word
		(*node)-> data = word;
		//Set branches to nullptr
		(*node)-> left = nullptr;
		(*node)->right = nullptr;
	}
	else  // if not empty
	{
		if(word < (*node)->data)
			insertHelper(&(*node)->left,word);
		else if(word > (*node)->data)
			insertHelper(&(*node)->right, word);
		else
			return;
	}
}

// Adds a word to the tree
void BinarySearchTree::insert(std::string word)
{
	insertHelper(&root, word);
}

void removeHelper(Node *node,std::string word)
{
	//Find the item
	bool found = false;
	Node* predecessor = nullptr;
	Node* current = node;
	if(current == nullptr)
		//std:cout << "Tree is empty"<<endl;
		return;
	while(current!=nullptr)
	{
		if(current->data==word)
		{
			found = true;
			break;
		}
		else
		{
			predecessor = current;
			if(word >(current->data))
				current = current->right;
			else
				current = current->left;
			
		}
	}
	if(!found)
	{
		return;
	}
	//Case 1:Removing a node with a single child
	if((current->left==nullptr && current->right != nullptr) || (current->left != nullptr && current->right==nullptr))
	{
		//Right leaf present, No left leaf
		if(current->left==nullptr && current->right != nullptr)
		{
			//If predecessor's left tree equals Node node
			if(predecessor->left==current)
			{
				//then predecessor's left tree becomes node's right tree
				//and delete node
				predecessor->left=current->right;
				delete current;
				current=nullptr;
			}
			else
			{
				//then predecessor's right tree becomes node's right tree
				//and delete node
				predecessor->right=current->right;
				delete current;
				current=nullptr;
			}
		}
		else //left leaf present, no right leaf present
		{
			if(predecessor->left == current)
			{
				predecessor->left = current->left;
				delete current;
				current = nullptr;
			}
		}
		return;
	}
	//Case 2: Removing a leaf node
	if(current->left == nullptr && current->right == nullptr)
	{
		if(predecessor->left==current)
			predecessor->left=nullptr;
		else
			predecessor->right=nullptr;
		delete current;
		return;
	}
	//Case 3: Node has two children
	//Replace Node with smallest value in right subtree
	if((current->left!=nullptr)&&(current->right!=nullptr))
	{
		Node* check = current->right;
		if((current->left==nullptr)&&(current->right==nullptr))
		{
			current=check;
			delete check;
			current->right=nullptr;
		}
		else // Right child has children
		{
			//If the node's right child has a left child
			//Move all the way down left to locate smallest element
			if((current->right)->left!=nullptr)
			{
				Node* leftCurrent;
				Node* leftCurrentPred;
				leftCurrentPred=current->right;
				leftCurrent=(current->right)->left;
				while(leftCurrent->left != nullptr)
				{
					leftCurrentPred=leftCurrent;
					leftCurrent=leftCurrent->left;
				}
				current->data=leftCurrent->data;
				delete leftCurrent;
				leftCurrentPred->left=nullptr;
			}
			else
			{
				Node* node = current->right;
				current->data=node->data;
				current->right=node->right;
				delete node;
			}
		}
		return;
	}
}

// Removes a word from the tree
void BinarySearchTree::remove(std::string word)
{
	removeHelper(root, word);
}

// Checks if a word is in the tree
bool BinarySearchTree::exists(std::string word) const
{
    Node* node = root;
	while(node != nullptr)
	{
		if(node->data == word) 
		{
			return true;
		}
		else
		{
			if (word > node->data)
			{
				node = node->right;
			}
			else
			{
				node = node->left;
			}
		}
	}
	return false;
}

//In order helper function
std::string inorderHelper(Node *root)
{
    if(root == nullptr)
    {
        return "";
    }
    else
    {
        return inorderHelper(root->left)
        + root->data + " "
        + inorderHelper(root->right);
    }
    
}

// Creates a string representing the tree in alphabetical orderr
std::string BinarySearchTree::inorder() const
{
    string  res = inorderHelper(root);
    int len = res.length();
    if(len >= 1 && res[len -1] == ' ')
    {
        res.pop_back();
    }
    return res;
}

//Preorder helper function
string preorderHelper(Node *root)
{
    if(root == nullptr)
    {
        return "";
    }
    else
    {
        return root->data + " "
		+ preorderHelper(root->left)
        + preorderHelper(root->right);
    }  
}
// Creates a string representing the tree in pre-order
std::string BinarySearchTree::preorder() const
{
    string  res = preorderHelper(root);
    int len = res.length();
    if(len >= 1 && res[len -1] == ' ')
    {
        res.pop_back();
    }
    return res;
}

//Postorder helper function
string postorderHelper(Node *root)
{
    if(root == nullptr)
    {
        return "";
    }
    else
    {
        return postorderHelper(root->left)
        + postorderHelper(root->right)
      	+root->data + " ";
	}
}

// Creates a string representing the tree in post-order
std::string BinarySearchTree::postorder() const
{
   string  res = postorderHelper(root);
    int len = res.length();
    if(len >= 1 && res[len -1] == ' ')
    {
        res.pop_back();
    }
    return res;
}

int sameTree(const Node *tree1, const Node *tree2)
{
	//both empty
	if(tree1==nullptr && tree2 == nullptr)
		return 1;
	
	//both non-empty ->compare them
	if(tree1!=nullptr && tree2!=nullptr)
	{
		return
		(
			tree1->data == tree2->data &&
			sameTree(tree1->left, tree2->left) &&
			sameTree(tree1->right, tree2->right)
		);
	}
	
	//one empty, one not -> false
	return 0;
}

// Checks if two trees are equal
bool BinarySearchTree::operator==(const BinarySearchTree &other) const
{
	if(sameTree(this->root,other.root))
	{
		return true;
	}
	else
	{
		return false;
	}

}

// Checks if two trees are not equal
bool BinarySearchTree::operator!=(const BinarySearchTree &other) const
{
    if(sameTree(this->root,other.root))
	{
		return true;
	}
	else
	{
		return false;
	}
}


// Reads in words from an input stream into the tree
std::istream& operator>>(std::istream &in, BinarySearchTree &tree)
{
	//create string for .insert function
	std::string s;
	
	//while there is input, add each element to the tree
	while(in >> s)
	{
		tree.insert(s);
	}
    return in;
}

// Writes the words, in-order, to an output stream
std::ostream& operator<<(std::ostream &out, const BinarySearchTree &tree)
{
	out<<tree.inorder();
		return out;
}