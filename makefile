courseworkA:
    cl /EHsc test.cpp BinarySearchTree.lib 
courseworkB:
	cl /EHsc spellChecker.cpp BinarySearchTree.lib
BinarySearchTree:
	cl /EHsc BinarySearchTree.cpp
library:
	lib BinarySearchTree.obj 
clean:
	del *.obj
	del *.exe
	del *.asm
test:
	test.exe
sentences:
	spellChecker -d dictionary.txt -i sentences_test.txt 
singleWords:
	spellChecker -d dictionary.txt -i single_words_test.txt
default:
	spellChecker.exe
