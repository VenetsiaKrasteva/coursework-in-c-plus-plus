Author: Venetsia Krasteva 
Matriculation number: 40313507
Tool chain for building - Notepad++ v7.5.4 (32-bit)
Running on: Visual Studio 2017 Tools Command Prompt (Developer Command Prompt for Vs 2017 (DEV))

Explaining how to use the Binary Search Tree
	1) Open Developer Command Prompt for Vs 2017 (DEV)
	2) Go to the directory of the files and with "cd <directory>" change it
	2) Type nmake BinarySearchTree to create BinarySearchTree.obj
	3) Type nmake library
	4) Choose one of the options:
		I)Test File
			1. type nmake courseworkA
			2. type nmake test
		II)Spell Checker
			1.type nmake courseworkB
			2.Choose one of the options:
				A) nmake default
				B) nmake sentences:
					spellChecker -d dictionary.txt -i sentences_test.txt 
				C) nmake singleWords
					spellChecker -d dictionary.txt -i single_words_test.txt
	5) To delete all of the *.obj/ *.exe/ .*asm type nmake clean
	 
A quick overview:
"bst.h" and "BinarySearchTree.cpp" are the files that create the Binary Search Tree(BST) itself.
In "bst.h" we declare functions and in "BinarySearchTree.cpp" we make the functions that we use.
PartA: In Part A we are creating and testing BST.
PartB: We are inserting a words from a dictionary into the BST converted to lower case and without punctuation and inserting words from another file into a vector converted to lower case and without punctuation to check if the words are in the dictionary, if they aren't, they are misspelled.